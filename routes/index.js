var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'IoT Logo' });
});


router.get('/picture.jpg', function(req, res, next) {
    res.download('./picture.jpg')
});
module.exports = router;