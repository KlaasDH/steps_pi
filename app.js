var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var socketIo = require('socket.io')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var favicon = require('serve-favicon');
var path = require('path');

var mariadb = require('mariadb');
var gpio = require("onoff").Gpio;

var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://test.mosquitto.org:1883')

var numberOfClientsConnected = 0;

var pin2Status = false;
var pin3Status = false;
var turning = false;
var direction = 'not moving';

var GPIO_In_2 = new gpio(2, 'in', 'both');


function stop_auto() {
    //stoppen
    if (direction === 'Forward' && GPIO_In_3.readSync()) { STOP(); } else if (direction === 'Backward' && GPIO_In_2.readSync()) { STOP() }

}

GPIO_In_2.watch(function(err, value) {
    if (value) {
        pin2Status = true;
        console.log(`PIN2: ${pin2Status}`)
        stop_auto()
    } else {
        pin2Status = false;
        console.log(`PIN2: ${pin2Status}`);
    }

});

var GPIO_In_3 = new gpio(3, 'in', 'both');
GPIO_In_3.watch(function(err, value) {
    if (value) {
        pin3Status = true;
        console.log(`PIN3: ${pin3Status}`);
        stop_auto()
    } else {
        pin3Status = false;
        console.log(`PIN3: ${pin3Status}`);
    }

});

var relais1 = new gpio(24, 'out');
var relais2 = new gpio(25, 'out');

function CheckStops() {
    console.log('Checking');
}


function stepForward() {
    if (turning) {
        io.emit('DIRChanged', `${direction}`);
    } else {
        relais1.writeSync(1);
        relais2.writeSync(0);
        turning = true;
        direction = 'Forward';
        asyncFunction(direction);
        io.emit('DIRChanged', `${direction}`);
    }
    client.publish('STEPS/stateChanged', `${direction}`)
}

function stepBackward() {
    if (turning) {
        io.emit('DIRChanged', `${direction}`);
    } else {
        relais1.writeSync(0);
        relais2.writeSync(1);
        turning = true;
        direction = 'Backward';
        asyncFunction(direction);
        io.emit('DIRChanged', `${direction}`);
    }
    client.publish('STEPS/stateChanged', `${direction}`)
}

const exitHook = require('exit-hook');

exitHook(() => {
    console.log('About to exit.');
    STOP();
});

function STOP() {
    if (turning) {
        direction = 'Not moving';
        relais1.writeSync(0);
        relais2.writeSync(0);
        turning = false;
        io.emit('DIRChanged', `${direction}`);
    } else {
        io.emit('DIRChanged', `${direction}`);
    }
    client.publish('STEPS/stateChanged', `${direction}`)
}






//MQTT
var connected = false;

client.on('connect', () => {
    client.subscribe('STEPS/connected');
    client.subscribe('STEPS/stateChanged');
    console.log("subsribed")
})

client.on('message', (topic, message) => {
    switch (topic) {
        case 'STEPS/connected':
            return handleMQTTConnected(message);
        case 'STEPS/stateChanged':
            return handleMQTTStateChanged(message);
    }
    console.log('No handler for topic %s', topic);
})


function handleMQTTConnected(message) {
    connected = (message.toString() === 'true');
    console.log('MQTT connected status %s', connected);
    if (connected)
        client.publish('STEPS/connected', 'connected')
}

function handleMQTTStateChanged(message) {
    console.log('MQTT received: %s', message);
    if (message.toString() === 'AUTO') {
        console.log(`MQTT: AUTO`);
        if (GPIO_In_2.readSync() && !GPIO_In_3.readSync()) {
            console.log('going forward')
            stepForward();
        } else if (GPIO_In_3.readSync() && !GPIO_In_2.readSync()) {
            console.log('going backwards')
            stepBackward();
        }
    }
}




//DB
const pool = mariadb.createPool({
    host: 'sql7.freesqldatabase.com',
    port: '3306',
    user: 'sql7338747',
    password: 'ZrVHVCuYYf',
    database: 'sql7338747',
    connectionLimit: 4
});
async function asyncFunction(data) {
    console.log(`db: ${data}`);
    let conn;
    var date = new Date();
    console.log(date);
    try {
        conn = await pool.getConnection();
        console.log('connected')

        const res = await conn.query(`INSERT INTO steps (direction, time) VALUES ('${data}', '${date}')`);
        console.log(res); // { affectedRows: 1, insertId: 1, warningStatus: 0 }

        const rows = await conn.query("SELECT * FROM steps ORDER BY id DESC LIMIT 30");
        //console.log(rows);
        io.emit('DbChanged', rows);
    } catch (err) {
        throw err;
    } finally {
        if (conn) return conn.release();
    }
}






var app = express();

// Socket.io
var io = socketIo();
app.io = io

// catch buttonClick
io.on('connection', function(socket) {
    console.log('A user connected');
    numberOfClientsConnected++;
    console.log(`Clients: ${numberOfClientsConnected}`);
    io.emit('DIRChanged', `${direction}`);
    socket.on('AUTO', function() {
        console.log(`btn clicked: AUTO`);
        if (GPIO_In_2.readSync() && !GPIO_In_3.readSync()) {
            console.log('going forward')
            stepForward();
        } else if (GPIO_In_3.readSync() && !GPIO_In_2.readSync()) {
            console.log('going backwards')
            stepBackward();
        }

    });
    socket.on('disconnect', function() {
        console.log('A user disconnected')
        numberOfClientsConnected--;
        console.log(`Clients: ${numberOfClientsConnected}`);
    });
});



// webcam
var NodeWebcam = require("node-webcam");
var Webcam = NodeWebcam.create({
    callbackReturn: "base64",
    saveShots: false
});
setupWebcam();

function setupWebcam() {
    function capture() {
        Webcam.capture("picture", function(err, data) {
            if (err) {
                throw err;
            }
            if (numberOfClientsConnected >= 1) {
                io.emit('pictTaken', data);
            }
            setTimeout(capture, 500);
        });
    }
    capture();
}


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

//favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;